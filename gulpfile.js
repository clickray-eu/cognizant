var gulp = require('gulp');
var babel = require('gulp-babel');
var replace = require('gulp-replace');
var sass = require('gulp-sass');
var watch = require('gulp-watch');
var concat = require('gulp-concat-util');
var plumber = require('gulp-plumber');
var autoprefixer = require('gulp-autoprefixer');
var replace = require('gulp-replace');
var sourcemaps = require('gulp-sourcemaps');
var sassGlob = require('gulp-sass-glob');
var browserSync = require('browser-sync').create();
var jsSource, scssSource;
var download = require("gulp-download");
var rename = require("gulp-rename");

scssSource = ['scss/template.scss','scss/*/*.scss','scss/*/*/*.scss','scss/*.scss'];
jsSource = ['js/*.js','js/*/*.js','js/*/*/*.js'];
config =['config.js'];

var user=process.argv[3];
if(user==undefined){
    user="global";
}

var plugins = require('gulp-load-plugins')();
var map = require('map-stream');
var events = require('events');
var emitter = new events.EventEmitter();
var path = require('path');
var gutil = require('gulp-util');
var currentTask="";




console.log("");
console.log("");
console.log("##############################################");
console.log('PLIKI BEDA GENEROWANE W KATALOGU : build/'+user);
console.log("##############################################");

gulp.task('scss', function() {
    currentTask="scss";
    var versionDate = new Date();
    gulp.src("scss/template.scss")
        .pipe(sassGlob({
              ignorePaths: [
                  '_slicknav.scss',
                  '_hubspot_styles.scss',
                  '_slick.scss',
                  '_variables.scss'
              ]
          }))
        .pipe(sourcemaps.init())
        .on('error', swallowError)
        .pipe(sass())
        .on('error', swallowError)
        .pipe(replace('{date}', versionDate))
        .on('error', swallowError)
        .pipe(autoprefixer({ browsers: ['>0%']}))
        .on('error', swallowError)
        .pipe(replace("/*==",""))
        .pipe(replace("==*/",""))
        .pipe(sourcemaps.write('.'))
        .pipe(gulp.dest('build/'+user+'/css'))
        .pipe(browserSync.stream({match: '**/template.css'}));
});
gulp.task('init_server',function(){
        currentTask="init_server";

    browserSync.init({
        server: {
            baseDir: 'build/'+user+'/',
            files: ['build/'+user+'/css/template.css','build/'+user+'/css/template.js']
        },
        open: false
    });
});
gulp.task('js', function() {
    
        currentTask="js";
    return gulp.src(jsSource)
        .pipe(sourcemaps.init())
        .pipe(concat.header('// file: <%= file.relative %>\n'))
        .pipe(concat.footer('\n// end file: <%= file.relative %>\n'))
        .pipe(concat('template.js'))   
        .pipe(plugins.jshint())
        .pipe(plugins.jshint.reporter('jshint-stylish'))     
        .on('error', swallowError)
        .pipe(babel())
        .on('error', swallowError)
        .pipe(sourcemaps.write('.'))
        .pipe(gulp.dest('build/'+user+'/js'));
});

gulp.task('config_change',function(){
    currentTask="config_change";
    try{
        var fs = require('fs');
        fs.readFile('./config.js', function (err, data) {
        try{            
            var config_json=JSON.parse(data.toString());
            var replace_js= new RegExp("src[=][\"\'](.*?)("+config_json.js_replace+")[^\"\']*[\"\']","g");
            var replace_css= new RegExp("href[=][\"\'](.*?)("+config_json.css_replace+")[^\"\']*[\"\']","g");
            if(config_json.download_url[config_json.download_url.length-1]=='/'){
                config_json.download_url=config_json.download_url.substr(0,config_json.download_url.length-1);
            }
            if(config_json.download_url.search("https")==0)
                config_json.download_url=config_json.download_url.replace('https','http');
            download(config_json.download_url)
                .on('error', swallowError)
                .pipe(rename("index.html"))
                .pipe(replace(replace_css,"href='css/template.css'"))
                .pipe(replace(replace_js,"src='js/template.js'"))
                .pipe(gulp.dest('build/'+user+'/'));
        }catch(err){
            swallowError(err);
        }
        });
    }catch(err){
        swallowError(err);
    }
        
});
    


gulp.task('default', ['init_server','scss','js','config_change'], function() {
    watch(scssSource,function(){
        gulp.start('scss');
    });
    watch(jsSource,function(){
        gulp.start('js');
    });
    watch(config,function(){
       gulp.start('config_change');
     });

    watch("build/"+user+"/index.html",function(){
       browserSync.reload();
    });
    gulp.watch("build/"+user+"/js/template.js").on('change', browserSync.reload);
});

var swallowError = function (error) {
 
    var lineNumber = error.line;
    var pluginName = "["+currentTask+"]";
    if(currentTask=="scss"){
        plugins.notify({
            title: 'Failed: ' +pluginName+ " ["+error.relativePath+"] on line "+lineNumber,
            message: error.messageOriginal
        }).write(error);
    }else if(currentTask=="js"){
        plugins.notify({
            title: 'Failed: ' +pluginName + "[js/template.js]",
            message: error.messageOriginal
        }).write(error);        
    } else if(currentTask=="config_change") {
        plugins.notify({
            title: 'Failed: ' +pluginName + "[config.js]",
            message: error.message
        }).write(error);        

    }
 
    gutil.beep();
 
    var report = '';
    var chalk = gutil.colors.white.bgRed;
 
    report += chalk('TASK:') + pluginName+'\n';
    report += chalk('ERROR:') + ' ' + error.message + '\n';
    if (error.line) { report += chalk('LINE:') + ' ' + error.line + '\n'; }
    if (error.fileName) { report += chalk('FILE:') + ' ' + error.fileName + '\n'; }
 
    console.error(report);
    if(currentTask!="config_change")
        this.emit('end');
}
